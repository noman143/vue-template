export default class Auth {
  static setToken (token) {
    localStorage.setItem('auth_token', token)
  }
  static getToken () {
    return localStorage.getItem('auth_token')
  }
}
