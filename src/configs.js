// import axios from 'axios'
const configs = {
  BASE_URL: null,
  NODE_ENV: null
}

switch (process.env.NODE_ENV) {
  case 'development':
    configs.BASE_URL = 'http://192.168.33.22/app/public/api/'
    break
  case 'production':
    configs.BASE_URL = 'http://api.emrevo.base.url'
    break
  default:
    configs.BASE_URL = 'http://api.emrevo.base.url'
    break
}

export default configs
