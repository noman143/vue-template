import nomi from './nomi'

/**
 * register your custom validator rules here
 */
export default [

  {name: 'nomi', validator: nomi}

]
