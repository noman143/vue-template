import Vue from 'vue'
import App from './App'
import router from './router'
import Auth from './auth'
import plugins from './plugins/plugins'
import { Validator } from 'vee-validate'
import customValidationRules from './custom-validators/validators'
import customValidationMessages from './custom-validators/custom-messages'

Vue.config.productionTip = false

/**
 * Register all the available plugins
 */
plugins.forEach(plugin => Vue.use(plugin))
/** -------------------------------- */

/**
 * Register all the custom validation rules
 */
customValidationRules.forEach(rule => {
  Validator.extend(rule.name, rule.validator)
})

/**
 * Register all custom validation messages
 */
Validator.localize(customValidationMessages)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
  mounted: function () {
    Auth.setToken('$2y$10$Y1vrE4iKRl7LlWu3qcMmtOHhA3uaZCJMlYVL9wF879cTgGhwmUbD6')
  }
})
