import axios from '../wrappers/axios'

export default {
  install: (Vue) => {
    Object.defineProperty(Vue.prototype, '$axios', { value: axios })
  }
}
