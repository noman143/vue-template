import configs from '../configs'

export default {
  install: (Vue) => {
    Object.defineProperty(Vue.prototype, '$configs', {value: configs})
  }
}
