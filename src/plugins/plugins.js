import axios from './axios'
import configs from './configs'
import VeeValidate from 'vee-validate'

// List all your plugins here. plugins listed below will be automatically loaded to the vue app.
export default [
  axios,
  configs,
  VeeValidate
]
