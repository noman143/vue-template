import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import HelloNomi from '../components/HelloNomi.vue'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },

    {
      path: '/nomi',
      name: 'HelloNomi',
      component: HelloNomi
    }
  ]
})

router.beforeEach((to, from, next) => {
  setTimeout(() => {
    next()
  }, 0)
})
export default router
