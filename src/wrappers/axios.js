import axios from 'axios'
import configs from '../configs'
import Auth from '../auth'

class Axios {
  constructor () {
    this.core = axios.create({
      baseURL: configs.BASE_URL
    })
    this.updateToken()
  }

  updateToken () {
    this.core.defaults.headers.common['Authorization'] = Auth.getToken()
  }

  callAxios (configs) {
    switch (configs.method) {
      case 'get':
        return this.core.get(configs.route)
      case 'post':
        return this.core.post(configs.route, configs.data)
      case 'all':
        return this.core.all(configs.funcs)
    }
  }

  get (route) {
    return this.callAxios({method: 'get', route: route})
  }

  post (route, data) {
    return this.callAxios({method: 'post', route: route, data: data})
  }

  all (funcs) {
    return this.callAxios({method: 'all', funcs: funcs})
  }
}

export default new Axios()
